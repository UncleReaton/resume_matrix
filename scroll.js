window.addEventListener('wheel', scroll, {passive:false});
window.addEventListener('keydown', check_click, true);

let count = 0;
let delta = 0;
const max = 5;

function scroll(event) {
	event.preventDefault();
	delta = event.deltaY;
	if (delta > 0 && count < max) {
		count++;
	} else if (delta < 0 && count > 0) {
		count--;
	}
	hide_and_seek(count);
}

function check_click(event) {
	if (event.code == 'ArrowRight' && count < max) {
		count++;
	} else if (event.code == 'ArrowLeft' && count > 0) {
		count--;
	} else if (event.code == 'ArrowUp' && count < max) {
		count++;
	} else if (event.code == 'ArrowDown' && count > 0) {
		count--;
	}

	hide_and_seek(count);
}

const arrow_left = document.getElementById('arrow-left');
const arrow_right = document.getElementById('arrow-right');

arrow_left.onclick = function(){go_left()};
arrow_right.onclick = function(){go_right()};

function go_right() {
	if (count < max)
		count++;
	hide_and_seek(count);
}

function go_left() {
	if (count > 0)
		count--;
	hide_and_seek(count);
}

function hide_and_seek(get_count) {
	switch(get_count){
		case 0:
			show('#title');
			hide('#skills');
			break;
		case 1:	
			hide('#title');
			show('#skills');
			hide('#languages');
			break;
		case 2:
			hide('#skills');
			show('#languages');
			hide('#soft-skills');
			break;
		case 3:
			hide('#languages');
			show('#soft-skills');
			hide('#formation');
			break;
		case 4:
			hide('#soft-skills');
			show('#formation');
			hide('#choice');
			break;
		case 5:
			hide('#formation');
			show('#choice');
			hide('#contact');
	}
}

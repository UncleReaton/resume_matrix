function hide(id) {
	document.querySelector(id).style.visibility="hidden";
}

function show(id) {
	document.querySelector(id).style.visibility="visible";
}

show('#title');
show('#arrow-left');
show('#arrow-right');
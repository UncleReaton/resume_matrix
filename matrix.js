let canvas = document.querySelector("canvas");

let ctx = canvas.getContext("2d");
let width = canvas.width = window.innerWidth;
let height = canvas.height = window.innerHeight;
let str = "ぁぁしぢへねゎゃェ゜ソゲビトラマヴヵリミピドゾコエゝわややべのっじきあヶルムフナタゴォゞゐゅぺはつつすぎぃヷレメブニダサオゟゑゆほばづずくい";
let matrix = str.split("");
let font = 12;
let col = width / font;
let arr = [];

let start_the_matrix = 1;

for(let i = 0; i < col; i++) {
	arr[i] = 1;
}

function launch_matrix() {
	if (start_the_matrix) {
		ctx.fillStyle = "rgba(0,0,0,0.05)";
		ctx.fillRect(0, 0, width, height);
		ctx.fillStyle = "#66ff66";
		ctx.font = `${font}px system-ui`;

		for(let i = 0; i < arr.length; i++) {
			let txt = matrix[Math.floor(Math.random() * matrix.length)];
			ctx.fillText(txt, i * font, arr[i] * font);

			if(arr[i] * font > height && Math.random() > 0.975) {
				arr[i] = 0;
			}
			arr[i]++;
		}
	}
}

let interval = setInterval(launch_matrix, 20);

window.addEventListener("resize", () => {
	width = canvas.width = window.innerWidth;
	height = canvas.height = window.innerHeight;
	col = width / font;
	clearInterval(interval);
	for(let i = 0; i < col; i++) {
		arr[i] = 1;
	}
	interval = setInterval(launch_matrix, 20);
});
